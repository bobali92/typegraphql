import { ObjectType, Field, ID, Root } from "type-graphql";
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";

@ObjectType()
@Entity()
export class User extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  firstName: string;

  @Field()
  @Column()
  lastName: string;
  // create both field schema and database column
  @Field()
  @Column("text", { unique: true })
  email: string;

  // only create field on schema
  @Field()
  name(@Root() parent: User): string{
    return `${parent.firstName} ${parent.lastName}`;
  };

  // only create database column not showing on schema
  @Column()
  password: string;

  @Column("bool",{ default: false })
  confirmed: boolean;
  
}